package dathtanim.tanawat.lab6;

/*
 * Calculator
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class Calculator extends MyFrame {

	private static final long serialVersionUID = 1L;
	private JTextField inputText;
	JPanel panel = new JPanel();
	JPanel panelbutton = new JPanel(new GridLayout(4, 4, 10, 10));
	JPanel panelText = new JPanel(new FlowLayout());
	private JButton button0;
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	private JButton button7;
	private JButton button8;
	private JButton button9;
	private JButton buttonPlus;
	private JButton buttonMultiply;
	private JButton buttonMinus;
	private JButton buttonDivide;
	private JButton buttonMod;
	private JButton buttonEqual;

	public Calculator(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	protected static void createAndShowGUI() {

		Calculator cal = new Calculator("Simple Calculator");
		cal.addComponents();
		cal.setFrameFeatures();

	}

	private void addComponents() {

		inputText = new JTextField("0");
		inputText.setHorizontalAlignment(SwingConstants.RIGHT);
		inputText.setBackground(Color.LIGHT_GRAY);
		inputText.setEditable(false);
		button0 = new JButton("0");
		button1 = new JButton("1");
		button2 = new JButton("2");
		button3 = new JButton("3");
		button4 = new JButton("4");
		button5 = new JButton("5");
		button6 = new JButton("6");
		button7 = new JButton("7");
		button8 = new JButton("8");
		button9 = new JButton("9");
		buttonPlus = new JButton("+");
		buttonMinus = new JButton("-");
		buttonMultiply = new JButton("*");
		buttonDivide = new JButton("/");
		buttonMod = new JButton("%");
		buttonEqual = new JButton("=");

		this.add(inputText, BorderLayout.NORTH);

		panelbutton.add(createSquareJPanel(Color.PINK, 10, button0));
		panelbutton.add(createSquareJPanel(Color.PINK, 10, button1));
		panelbutton.add(createSquareJPanel(Color.PINK, 10, button2));
		panelbutton.add(createSquareJPanel(Color.PINK, 10, button3));
		panelbutton.add(createSquareJPanel(Color.PINK, 10, button4));
		panelbutton.add(createSquareJPanel(Color.PINK, 10, button5));
		panelbutton.add(createSquareJPanel(Color.PINK, 10, button6));
		panelbutton.add(createSquareJPanel(Color.PINK, 10, button7));
		panelbutton.add(createSquareJPanel(Color.PINK, 10, button8));
		panelbutton.add(createSquareJPanel(Color.PINK, 10, button9));
		panelbutton.add(createSquareJPanel(Color.GREEN, 10, buttonPlus));
		panelbutton.add(createSquareJPanel(Color.GREEN, 10, buttonMinus));
		panelbutton.add(createSquareJPanel(Color.GREEN, 10, buttonMultiply));
		panelbutton.add(createSquareJPanel(Color.GREEN, 10, buttonDivide));
		panelbutton.add(createSquareJPanel(Color.GREEN, 10, buttonMod));
		panelbutton.add(createSquareJPanel(Color.BLUE, 10, buttonEqual));

		this.add(panelbutton, BorderLayout.CENTER);

	}
}
