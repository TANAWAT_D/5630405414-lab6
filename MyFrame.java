package dathtanim.tanawat.lab6;

/*
 * MyFrame
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	protected MyFrame(String title) {
		super(title);
	}

	protected void setFrameFeatures() {

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setVisible(true);
		pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	protected JPanel createSquareJPanel(Color color, int size, JComponent comp) {

		JPanel panel = new JPanel();
		panel.setBackground(color);
		panel.setSize(size, size);
		panel.add(comp);
		return panel;

	}

}
