package dathtanim.tanawat.lab6;

/*
 * MySocialMedia
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class MySocialMedia extends MyFrame {

	private static final long serialVersionUID = 1L;
	private JTextArea area;
	private JButton buttonPost;
	JPanel panel = new JPanel();
	JPanel panelcombobox = new JPanel(new GridLayout(1, 3));

	protected MySocialMedia(String title) {
		super(title);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	protected static void createAndShowGUI() {

		MySocialMedia msm = new MySocialMedia("My Social Media");
		msm.addComponents();
		msm.setFrameFeatures();

	}

	private void addComponents() {

		area = new JTextArea("What's on your mind?", 5, 30);
		buttonPost = new JButton("Post");

		JComboBox<String> feeling = new JComboBox<String>();
		feeling.addItem("Excited");
		feeling.addItem("Happy");
		feeling.addItem("Sad");
		feeling.addItem("Angry");
		feeling.addItem("Thankful");
		feeling.addItem("Post");
		feeling.setEditable(true);

		JComboBox<String> whocansee = new JComboBox<String>();
		whocansee.addItem("Friends");
		whocansee.addItem("Public");
		whocansee.addItem("Only me");
		whocansee.setEditable(true);

		panelcombobox.add(createSquareJPanel(Color.YELLOW, 10, feeling));
		panelcombobox.add(createSquareJPanel(Color.GREEN, 10, whocansee));
		panelcombobox.add(createSquareJPanel(Color.BLUE, 10, buttonPost));
		this.add(area, BorderLayout.NORTH);
		this.add(panelcombobox, BorderLayout.SOUTH);

	}

}
